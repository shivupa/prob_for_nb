###############################################################################
#                                   IMPORTS                                   #
###############################################################################
import numpy as np
import scipy.linalg as spla
import matplotlib.pyplot as plt
import time
from datetime import datetime
import textwrap as tw
###############################################################################
#                                 DEFINITIONS                                 #
###############################################################################
###############################################################################
#                                  FUNCTIONS                                  #
###############################################################################
###############################################################################
#                               INPUT / OUTPUT                                #
###############################################################################
###############################################################################
#                                     RHF                                     #
###############################################################################
def nuclear_repulsion_energy(mol):
    return mol.energy_nuc()
def overlap(mol):
    return mol.intor('cint1e_ovlp_sph')
    #return mol.intor('cint1e_ovlp_cart')
def kinetic(mol):
    return mol.intor('cint1e_kin_sph')
    #return mol.intor('cint1e_kin_cart')
def nuclear_attraction(mol):
    return mol.intor('cint1e_nuc_sph')
    #return mol.intor('cint1e_nuc_cart')
def two_electron_integrals(mol):
    return mol.intor('cint2e_sph',aosym='s8')
    #return mol.intor('cint2e_cart',aosym='s8')
__idx2_cache = {}
def idx2(i, j):
    """2-index transformation for accessing elements of a symmetric matrix
    stored in upper-triangular form.
    """
    if (i, j) in __idx2_cache:
        return __idx2_cache[i, j]
    elif i >= j:
        __idx2_cache[i, j] = int(i*(i+1)/2+j)
    else:
        __idx2_cache[i, j] = int(j*(j+1)/2+i)
    return __idx2_cache[i, j]
def idx4(i, j, k, l):
    # if i < j:
    #     temp = j
    #     j = i
    #     i = temp
    # if k < l:
    #     temp = k
    #     k = l
    #     l = temp
    # ij = ((i*(i+1))/2) + j
    # kl = ((k*(k+1))/2) + l
    # if ij < kl:
    #     temp = ij
    #     ij = kl
    #     kl = temp
    # ijkl = ((ij*(ij+1))/2) + kl
    # return ijkl
    return idx2(idx2(i, j), idx2(k, l))
def form_density_matrix(C, num_ao, num_elec_alpha):
    D = np.zeros((num_ao,num_ao))
    for i in range(num_ao):
        for j in range(num_ao):
            for k in range(num_elec_alpha):
                D[i,j] +=  C[i,k] * C[j,k]
    return D
def rmsc_dm(D, D_last):
    return np.sqrt(np.sum((D - D_last)**2))
def diagonalize_fock(F, S):
    return spla.eigh(F,S)
def RHF_coulomb_exchange(D, eri, num_ao):
    G = np.zeros((num_ao,num_ao))
    for i in range(num_ao):
        for j in range(num_ao):
            for k in range(num_ao):
                for l in range(num_ao):
                    G[i,j] += D[k,l] * ((2.0*(eri[idx4(i,j,k,l)])) - (eri[idx4(i,k,j,l)]))
    return G
def RHF_electronic_energy(D, H, F, num_ao, num_elec_alpha):
    E = np.sum(np.multiply(D , (H +  F)))
    return E
def RHF(mol, makeplots = True, debug = False,print_hf = False):
    """
    Needs doc
    """
    # initialize molecule
    mol = setup_molecule(mol)
    # start timer
    rhf_start_time = time.time()
    # get size of basis and number of electrons
    num_ao = mol["num_atomic_orbital"]
    num_elec_alpha, num_elec_beta = mol["nelec"]
    # calculate nuclear repulsions energy (scalar)
    E_nuc = nuclear_repulsion_energy(mol)
    # calculate overlap integrals
    S = overlap(mol)
    # calculate kinetic energy integrals
    T = kinetic(mol)
    # calculate nuclear attraction integrals
    V = nuclear_attraction(mol)
    # form core Hamiltonian
    H = T + V
    # parameters for main loop
    iteration_max = 100
    convergence_E = 1e-9
    convergence_DM = 1e-5
    # loop variables
    iteration_num = 0
    E_total = 0
    E_elec = 0.0
    iteration_E_diff = 0.0
    iteration_rmsc_dm = 0.0
    converged = False
    # get two electron integrals
    eri = two_electron_integrals(mol)
    # set inital density matrix to zero
    D = np.zeros((num_ao,num_ao))
    if (print_hf == True):
        if (num_elec_alpha != num_elec_beta):
            print_start_HF(mol, num_ao, num_elec_alpha, num_elec_beta, iteration_max, convergence_E, convergence_DM, E_nuc, method = "RHF", warning = True)
            #return None, None
        else:
            print_start_HF(mol, num_ao, num_elec_alpha, num_elec_beta, iteration_max, convergence_E, convergence_DM, E_nuc, method = "RHF")
    #debugging stuff
    if makeplots:
        iteration_num_list = []
        iteration_time_list = []
        iteration_rmsc_dm_list = []
        iteration_E_diff_list = []
        E_elec_list = []
    # main iteration loop
    while(not converged):
        iteration_start_time = time.time()
        iteration_num += 1
        # store last iteration
        E_elec_last = E_elec
        D_last = np.copy(D)
        # form G matrix
        G = RHF_coulomb_exchange(D, eri, num_ao)
        # build fock matrix
        F  = H + G
        # calculate electronic energy
        E_elec = RHF_electronic_energy(D, H, F, num_ao, num_elec_alpha)
        E_total = E_elec + E_nuc
        # calculate energy change of iteration
        iteration_E_diff = np.abs(E_elec - E_elec_last)
        # solve the generalized eigenvalue problem
        E_orbitals, C = diagonalize_fock(F,S)
        # compute new density matrix
        D = form_density_matrix(C, num_ao, num_elec_alpha)
        # rms change of density matrix
        iteration_rmsc_dm = rmsc_dm(D, D_last)
        # test convergence
        if(np.abs(iteration_E_diff) < convergence_E and iteration_rmsc_dm < convergence_DM):
            converged = True
        if(iteration_num == iteration_max):
            converged = True
        iteration_end_time = time.time()
        # debugging stuff
        if makeplots:
            iteration_num_list.append(iteration_num)
            iteration_time_list.append(iteration_end_time - iteration_start_time)
            iteration_rmsc_dm_list.append(iteration_rmsc_dm)
            iteration_E_diff_list.append(iteration_E_diff)
            E_elec_list.append(E_elec)
        if (print_hf == True):
            print_iteration(iteration_num, iteration_start_time, iteration_end_time, iteration_rmsc_dm, iteration_E_diff, E_elec, E_total)
    rhf_end_time = time.time()
    if (print_hf == True):
            print_end_hf(E_total,iteration_num, iteration_rmsc_dm, iteration_E_diff,rhf_start_time,rhf_end_time, method = "RHF")
    # debugging stuff
    if makeplots:
        plot_fun(iteration_num_list, iteration_time_list, "iteration_time.eps", "iteration", "time(s)", "Iteration times")
        plot_fun(iteration_num_list, iteration_rmsc_dm_list, "iteration_rmsd_dm.eps", "iteration", "rmsc_dm", "Iteration change in dm")
        plot_fun(iteration_num_list, iteration_E_diff_list, "iteration_E_diff.eps", "iteration", "E", "Iteration change in E")
        plot_fun(iteration_num_list, E_elec_list, "iteration_E_elec.eps", "iteration", "E", "Iteration E_elec")
    if debug:
        print_comparison_rhf(E_total, mol)
    return E_total, C
###############################################################################
#                                  MAIN CODE                                  #
###############################################################################
if __name__ == '__main__':
    comment_structure("input / output")
