import pyscf
from pyscf import gto, scf
import numpy as np

def overlap(mol):
    return mol.intor('cint1e_ovlp_sph')
def kinetic(mol):
    return mol.intor('cint1e_kin_sph')
def nuclear_attraction(mol):
    return mol.intor('cint1e_nuc_sph')
def two_electron_integrals(mol):
    return mol.intor('cint2e_sph',aosym='1')
def nuclear_repulsion_energy(mol):
    return mol.energy_nuc()
mol = pyscf.gto.M(
    atom=[['H', (2., 3.46410162, 0.)],
          ['H', (-2.,  3.46410162,  0.)],
          ['H', (-4.0000000e+00,4.8985872e-16,  0.0000000e+00)],
          ['H', (-2. , -3.46410162,  0.)],
          ['H', (2., -3.46410162,  0.)],
          ['H', (4.00000000e+00, -4.53243112e-15,  0.00000000e+00)]],
    basis='sto-3g',
    unit = "bohr",
    verbose=0
)

print(overlap(mol))
print()
print(kinetic(mol))
print()
print(nuclear_attraction(mol))
print()
print(two_electron_integrals(mol))
print()
print(nuclear_repulsion_energy(mol))

mf = scf.RHF(mol)
mf.diis = False
E_pyscf = mf.kernel()
print('E(PYSCF) =', E_pyscf)
