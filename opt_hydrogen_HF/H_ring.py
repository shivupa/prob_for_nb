import numpy as np
import RHF
import matplotlib.pyplot as plt

def plot_h_ring(molecule):
    fig = plt.figure(facecolor='white',figsize=(3,3))
    ax = fig.add_subplot(111)
    fig.set_tight_layout(True)
    for atom in molecule:
        x = atom["pos"][0]
        y = atom["pos"][1]
        plt.plot(x,y,marker='.',markersize=20,linestyle=None,color = 'black')
    radius = 4.0
    theta = np.linspace(0,2*np.pi,100)
    x = radius * np.cos(theta)
    y = radius * np.sin(theta)
    plt.plot(x,y,linestyle="--",color="black")
    plt.savefig("hydrogen.eps")

def make_h_ring(num_atoms, radius, deformation, num_contracted):
    molecule = []
    for atom in range(num_atoms):
        curr_atom = dict()
        if atom % 2 == 0:
            deformation_amount = deformation
        else:
            deformation_amount = 1.0 + (1.0 - deformation)
        theta = (atom)*((2.0*np.pi)/(num_atoms))
        theta += ((2.0*np.pi)/(num_atoms))*(deformation_amount)
        x = radius * np.cos(theta)
        y = radius * np.sin(theta)
        z = 0.0
        curr_atom["element"] = "H"
        curr_atom["pos"] = np.array([x,y,z])
        curr_atom["basis"] = populate_basis_functions(num_contracted)
        # print(curr_atom)
        molecule.append(curr_atom)
    return molecule

def populate_basis_functions(num_contracted):
    # STO-2G
    """
    !  STO-2G  EMSL  Basis Set Exchange Library   4/11/18 11:33 AM
    ! Elements                             References
    ! --------                             ----------
    !  H - He: W.J. Hehre, R.F. Stewart and J.A. Pople, J. Chem. Phys. 2657 (1969).
    ! Li - Ne:
    ! Na - Ar: W.J. Hehre, R. Ditchfield, R.F. Stewart, J.A. Pople, J. Chem. Phys.
    !  K - Kr: 52, 2769 (1970).
    !


    ****
    H     0
    S   2   1.00
          1.309756377            0.430128498
          0.233135974            0.678913531
    ****
    """
    # STO-3G
    """
    !  STO-3G  EMSL  Basis Set Exchange Library   4/11/18 11:34 AM
    ! Elements                             References
    ! --------                             ----------
    !  H - Ne: W.J. Hehre, R.F. Stewart and J.A. Pople, J. Chem. Phys. 2657 (1969).
    ! Na - Ar: W.J. Hehre, R. Ditchfield, R.F. Stewart, J.A. Pople,
    !          J. Chem. Phys.  2769 (1970).
    ! K,Ca - : W.J. Pietro, B.A. Levy, W.J. Hehre and R.F. Stewart,
    ! Ga - Kr: J. Am. Chem. Soc. 19, 2225 (1980).
    ! Sc - Zn: W.J. Pietro and W.J. Hehre, J. Comp. Chem. 4, 241 (1983) + Gaussian.
    !  Y - Cd: W.J. Pietro and W.J. Hehre, J. Comp. Chem. 4, 241 (1983). + Gaussian
    !


    ****
    H     0
    S   3   1.00
          3.42525091             0.15432897
          0.62391373             0.53532814
          0.16885540             0.44463454
    ****
    """
    #STO-6G
    """
    !  STO-6G  EMSL  Basis Set Exchange Library   4/11/18 11:35 AM
    ! Elements                             References
    ! --------                             ----------
    ! H - Ne:  W.J. Hehre, R.F. Stewart and J.A. Pople, J. Chem. Phys. 51, 2657
    ! (1969).
    ! Na - Ar:  W.J. Hehre, R. Ditchfield, R.F. Stewart and J.A. Pople,
    ! J. Chem. Phys. 52, 2769 (1970).
    !



    ****
    H     0
    S   6   1.00
         35.52322122             0.00916359628
          6.513143725            0.04936149294
          1.822142904            0.16853830490
          0.625955266            0.37056279970
          0.243076747            0.41649152980
          0.100112428            0.13033408410
    ****
    """
    basis = dict()
    basis["num_shells"] = 1
    if num_contracted == 2:
        basis["exponents"] = np.array([1.309756377,0.233135974])
        basis["contraction coefficients"] = np.array([0.430128498,0.678913531])


if __name__ == '__main__':
    num_atoms = 6
    radius = 4.0 # angstroms
    deformation = 0.75 # ratio (must be less than 1)
    num_contracted = 2
    H_ring = make_h_ring(num_atoms, radius, deformation, num_contracted)
    # RHF(H_ring)
    # print(H_ring)
    for i in H_ring:
        print(i)
    plot_h_ring(H_ring)
