import numpy as np
import scipy.linalg as spla
import matplotlib.pyplot as plt
import pyscf
from pyscf import gto, scf
import time
from datetime import datetime
import textwrap as tw
##############################################################
# Functions
##############################################################
##############################################################
# I/O
##############################################################
LINE_LENGTH = 79
#############################
# General
#############################
def print_startup():
    # progam info
    print("*"*LINE_LENGTH)
    print("*"*LINE_LENGTH)
    print("*{:^77}*".format(""))
    print("*{:^77}*".format("HARTREE-FOCK PROGRAM"))
    print("*{:^77}*".format("v 1.0"))
    print("*{:^77}*".format("SHIV UPADHYAY"))
    print("*{:^77}*".format("LAST UPDATED : Dec. 25, 2016"))
    print("*{:^77}*".format(""))
    print("*"*LINE_LENGTH)
    # input block
    print("*"*LINE_LENGTH)
    print("*{:^77}*".format(""))
    print("*{:^77}*".format("INPUT"))
    print("*{:^77}*".format(""))
    print("*"*LINE_LENGTH)
def print_molecule(mol):
    for i in mol.atom:
        print("{:^79}".format("{:>3} {:> 16f} {:> 16f} {:> 16f}".format(i[0], i[1][0], i[1][1], i[1][2])))
def print_iteration(iteration_num, iteration_start_time, iteration_end_time, iteration_rmsc_dm, iteration_E_diff, E_elec, E_total):
    print("{:^79}".format("{:>4d} {:>11f} {:>11f} {:>11f} {:>11f} {:>11f}".format(iteration_num, iteration_end_time - iteration_start_time, iteration_rmsc_dm, iteration_E_diff, E_elec, E_total)))
#############################
# RHF
#############################
def print_hf_input(mol, num_ao, num_elec_alpha, num_elec_beta, iteration_max, convergence_E, convergence_DM, E_nuc, method, warning):
    print("{:^79}".format("{:<25} : {:>25}".format("Method", method)))
    print("{:^79}".format("{:<25} : {:>25}".format("Structure", "")))
    print_molecule(mol)
    print("{:^79}".format("{:<25} : {:>25}".format("Nuclear repulsion energy", E_nuc)))
    print("{:^79}".format("{:<25} : {:>25}".format("Number of atomic orbitals", num_ao)))
    print("{:^79}".format("{:<25} : {:>25}".format("Number of alpha electrons", num_elec_alpha)))
    print("{:^79}".format("{:<25} : {:>25}".format("Number of beta electrons", num_elec_beta)))
    print("{:^79}".format("{:<25} : {:>25}".format("Maximum iterations", iteration_max)))
    print("{:^79}".format("{:<25} : {:>25}".format("Convergence E", convergence_E)))
    print("{:^79}".format("{:<25} : {:>25}".format("Convergence DM", convergence_DM)))
    print("*"*LINE_LENGTH)
    if warning:
        print_rhf_warning()
        return
    else:
        print("*{:^77}*".format(""))
        print("*{:^77}*".format("STARTING ITERATIONS"))
        print("*{:^77}*".format(""))
        print("*"*LINE_LENGTH)
        print("{:^79}".format("{:>04} {:>11} {:>11} {:>11} {:>11} {:>11}".format("Iter", "Time(s)", "RMSC DM", "delta E", "E_elec", "E_total")))
        print("{:^79}".format("{:>04} {:>11} {:>11} {:>11} {:>11} {:>11}".format("****", "*******", "*******", "*******", "******", "*******")))
def print_start_HF(mol, num_ao, num_elec_alpha, num_elec_beta, iteration_max, convergence_E, convergence_DM, E_nuc, method, warning = False):
    print_startup()
    print_hf_input(mol, num_ao, num_elec_alpha, num_elec_beta, iteration_max, convergence_E, convergence_DM, E_nuc, method, warning)
def print_end_hf(E_total, iteration_num, iteration_rmsc_dm, iteration_E_diff, start_time, end_time, method):
    print("*"*LINE_LENGTH)
    print("*{:^77}*".format(""))
    print("*{:^77}*".format("FINISHED {}".format(method)))
    print("*{:^77}*".format(""))
    print("*"*LINE_LENGTH)
    print("{:^79}".format("{:<25} : {:>25}".format("Iterations", iteration_num)))
    print("{:^79}".format("{:<25} : {:>25}".format("Final RMSC DM", iteration_rmsc_dm)))
    print("{:^79}".format("{:<25} : {:>25}".format("Final delta E", iteration_E_diff)))
    print("{:^79}".format("{:<25} : {:>25}".format("Final E", E_total)))
    print("{:^79}".format("{:<25} : {:>25}".format("Time(s)", end_time - start_time)))
    print("{:^79}".format("{:<25} : {:>25}".format("Date (m/d/y H:M)", '{:%m-%d-%Y %H:%M}'.format(datetime.today()))))
    print("*"*LINE_LENGTH)
    print("*"*LINE_LENGTH)
def print_comparison_rhf(E_total, mol):
    print("\n\n\nDEBUG")
    print('E(Mine) =', E_total)
    mf = scf.RHF(mol)
    E_pyscf = mf.kernel()
    print('E(PYSCF) =', E_pyscf)
    diff = E_total - E_pyscf
    print("difference = ", diff)
    if diff < 1e-10:
        print("OK")
    else:
        print("NOT OK")
def print_rhf_warning():
    print("\n")
    print("{:^79}".format("*"*(3*LINE_LENGTH/4)))
    print("{:^79}".format("WARNING:"))
    print("{:^79}".format("*"*(3*LINE_LENGTH/4)))
    warning_text = "RHF WARNING: Number of alpha and beta electrons are not equal. RHF may not be the best method to treat this system."
    warning_lines = tw.wrap(warning_text,(LINE_LENGTH/2))
    for i in warning_lines:
        print("{:^79}".format(i))
    print("{:^79}".format("*"*(3*LINE_LENGTH/4)))
    print("\n")
def plot_fun(x,y,plot_name,xlab,ylab,title):
    plt.rc('font', family='serif', size=24)
    colors = ['#B933AD','#0039A6','#00933C','#FCCB09','#FF6319','#EE352E']
    xminimum = np.min(x)
    xmaximum = np.max(x)
    yrange = np.max(y) - np.min(y)
    yminimum = np.min(y) - 0.1*yrange
    ymaximum = np.max(y) + 0.1*yrange
    fig = plt.figure(facecolor='white', figsize=(8, 8))
    ax = fig.add_subplot(111)
    plt.xlim(xminimum, xmaximum)
    plt.ylim(yminimum, ymaximum)
    plt.grid(True)
    plt.plot(x, y, color=colors[np.random.randint(0,len(colors))], linewidth=2.0)
    plt.plot([xminimum, xmaximum], [0, 0],color='black', linestyle='-', linewidth=0.5)
    plt.plot([0, 0], [yminimum, ymaximum],color='black', linestyle='-', linewidth=0.5)
    for axis in ['top', 'bottom', 'left', 'right']:
        ax.spines[axis].set_linewidth(3)
    plt.minorticks_on()
    ax.xaxis.set_tick_params(which = "major", width=0.5, length=10)
    ax.yaxis.set_tick_params(which = "major", width=0.5, length=10)
    ax.xaxis.set_tick_params(which = "minor", width=0.5, length=5)
    ax.yaxis.set_tick_params(which = "minor", width=0.5, length=5)
    plt.xlabel(xlab)
    plt.ylabel(ylab)
    plt.title(title, y=1.08)
    fig.set_tight_layout(True)
    fig.savefig(plot_name)
##############################################################
# General
##############################################################
def nuclear_repulsion_energy(mol):
    return mol.energy_nuc()
def overlap(mol):
    return mol.intor('cint1e_ovlp_sph')
    #return mol.intor('cint1e_ovlp_cart')
def kinetic(mol):
    return mol.intor('cint1e_kin_sph')
    #return mol.intor('cint1e_kin_cart')
def nuclear_attraction(mol):
    return mol.intor('cint1e_nuc_sph')
    #return mol.intor('cint1e_nuc_cart')
def two_electron_integrals(mol):
    return mol.intor('cint2e_sph',aosym='s8')
    #return mol.intor('cint2e_cart',aosym='s8')
__idx2_cache = {}
def idx2(i, j):
    """2-index transformation for accessing elements of a symmetric matrix
    stored in upper-triangular form.
    """
    if (i, j) in __idx2_cache:
        return __idx2_cache[i, j]
    elif i >= j:
        __idx2_cache[i, j] = int(i*(i+1)/2+j)
    else:
        __idx2_cache[i, j] = int(j*(j+1)/2+i)
    return __idx2_cache[i, j]
def idx4(i, j, k, l):
    # if i < j:
    #     temp = j
    #     j = i
    #     i = temp
    # if k < l:
    #     temp = k
    #     k = l
    #     l = temp
    # ij = ((i*(i+1))/2) + j
    # kl = ((k*(k+1))/2) + l
    # if ij < kl:
    #     temp = ij
    #     ij = kl
    #     kl = temp
    # ijkl = ((ij*(ij+1))/2) + kl
    # return ijkl
    return idx2(idx2(i, j), idx2(k, l))
def form_density_matrix(C, num_ao, num_elec_alpha):
    D = np.zeros((num_ao,num_ao))
    for i in range(num_ao):
        for j in range(num_ao):
            for k in range(num_elec_alpha):
                D[i,j] +=  C[i,k] * C[j,k]
    return D
def rmsc_dm(D, D_last):
    return np.sqrt(np.sum((D - D_last)**2))
def diagonalize_fock(F, S):
    # can use spla.eigh for speed up but I want to
    # transition to complex eigenvectors
    # return spla.eig(F,S,right=True)
    return spla.eigh(F,S)
    # s_eig, L = spla.eigh(S)
    # s = 1.0/np.sqrt(s_eig)
    # s_one_half = np.diag(s)
    # X = np.dot(L,np.dot(s_one_half, L))
    # F_prime = np.dot(X,np.dot(F, X))
    # e, c_prime =  spla.eigh(F_prime)
    # return e, np.dot(X,c_prime)
#############################
# RHF
#############################
def RHF_coulomb_exchange(D, eri, num_ao):
    G = np.zeros((num_ao,num_ao))
    for i in range(num_ao):
        for j in range(num_ao):
            for k in range(num_ao):
                for l in range(num_ao):
                    G[i,j] += D[k,l] * ((2.0*(eri[idx4(i,j,k,l)])) - (eri[idx4(i,k,j,l)]))
    return G
def RHF_electronic_energy(D, H, F, num_ao, num_elec_alpha):
    E = np.sum(np.multiply(D , (H +  F)))
    return E
##############################################################
# Methods
##############################################################
#############################
# RHF
#############################
def RHF(mol, makeplots = True, debug = False,print_hf = False):
    """
    Modeled after Evangelista CHEM532 Programming Project
    http://www.evangelistalab.org/wp-content/uploads/2013/12/Hartree-Fock-Theory.pdf
    """
    rhf_start_time = time.time()
    # get size of basis and number of electrons
    num_ao = mol.nao_nr()
    num_elec_alpha, num_elec_beta = mol.nelec
    # calculate nuclear repulsions energy (scalar)
    E_nuc = nuclear_repulsion_energy(mol)
    # calculate overlap integrals
    S = overlap(mol)
    # calculate kinetic energy integrals
    T = kinetic(mol)
    # calculate nuclear attraction integrals
    V = nuclear_attraction(mol)
    # form core Hamiltonian
    H = T + V
    # parameters for main loop
    iteration_max = 100
    convergence_E = 1e-9
    convergence_DM = 1e-5
    # loop variables
    iteration_num = 0
    E_total = 0
    E_elec = 0.0
    iteration_E_diff = 0.0
    iteration_rmsc_dm = 0.0
    converged = False
    # get two electron integrals
    eri = two_electron_integrals(mol)
    # set inital density matrix to zero
    D = np.zeros((num_ao,num_ao))
    if (print_hf == True):
        if (num_elec_alpha != num_elec_beta):
            print_start_HF(mol, num_ao, num_elec_alpha, num_elec_beta, iteration_max, convergence_E, convergence_DM, E_nuc, method = "RHF", warning = True)
            #return None, None
        else:
            print_start_HF(mol, num_ao, num_elec_alpha, num_elec_beta, iteration_max, convergence_E, convergence_DM, E_nuc, method = "RHF")
    #debugging stuff
    if makeplots:
        iteration_num_list = []
        iteration_time_list = []
        iteration_rmsc_dm_list = []
        iteration_E_diff_list = []
        E_elec_list = []
    # main iteration loop
    while(not converged):
        iteration_start_time = time.time()
        iteration_num += 1
        # store last iteration
        E_elec_last = E_elec
        D_last = np.copy(D)
        # form G matrix
        G = RHF_coulomb_exchange(D, eri, num_ao)
        # build fock matrix
        F  = H + G
        # calculate electronic energy
        E_elec = RHF_electronic_energy(D, H, F, num_ao, num_elec_alpha)
        E_total = E_elec + E_nuc
        # calculate energy change of iteration
        iteration_E_diff = np.abs(E_elec - E_elec_last)
        # solve the generalized eigenvalue problem
        E_orbitals, C = diagonalize_fock(F,S)
        # compute new density matrix
        D = form_density_matrix(C, num_ao, num_elec_alpha)
        # rms change of density matrix
        iteration_rmsc_dm = rmsc_dm(D, D_last)
        # test convergence
        if(np.abs(iteration_E_diff) < convergence_E and iteration_rmsc_dm < convergence_DM):
            converged = True
        if(iteration_num == iteration_max):
            converged = True
        iteration_end_time = time.time()
        # debugging stuff
        if makeplots:
            iteration_num_list.append(iteration_num)
            iteration_time_list.append(iteration_end_time - iteration_start_time)
            iteration_rmsc_dm_list.append(iteration_rmsc_dm)
            iteration_E_diff_list.append(iteration_E_diff)
            E_elec_list.append(E_elec)
        if (print_hf == True):
            print_iteration(iteration_num, iteration_start_time, iteration_end_time, iteration_rmsc_dm, iteration_E_diff, E_elec, E_total)
    rhf_end_time = time.time()
    if (print_hf == True):
            print_end_hf(E_total,iteration_num, iteration_rmsc_dm, iteration_E_diff,rhf_start_time,rhf_end_time, method = "RHF")
    # debugging stuff
    if makeplots:
        plot_fun(iteration_num_list, iteration_time_list, "iteration_time.eps", "iteration", "time(s)", "Iteration times")
        plot_fun(iteration_num_list, iteration_rmsc_dm_list, "iteration_rmsd_dm.eps", "iteration", "rmsc_dm", "Iteration change in dm")
        plot_fun(iteration_num_list, iteration_E_diff_list, "iteration_E_diff.eps", "iteration", "E", "Iteration change in E")
        plot_fun(iteration_num_list, E_elec_list, "iteration_E_elec.eps", "iteration", "E", "Iteration E_elec")
    if debug:
        print_comparison_rhf(E_total, mol)
    return E_total, C
##############################################################
# Input
##############################################################
#zmat
best_experimental = """O
H 1 0.95721
H 1 0.95721 2 104.522"""
structure = """O	 0.0000000	 0.0000000	 0.0000000
H	 0.7569685	 0.0000000	-0.5858752
H	-0.7569685	 0.0000000	-0.5858752"""
# structure = """H\nH 1 0.7414"""
#pyscf mol object

mol1 = pyscf.gto.M(
    atom=structure,
    #basis='ccpvdz',
    #basis='6-31G',
    basis='sto-3g',
    unit = "Ang",
    verbose=0,
    symmetry=False
)
#print "ov"
#print overlap(mol)
#print "kin"
#print kinetic(mol)
#print "na"
#print nuclear_attraction(mol)
# print "two electron"
# print two_electron_integrals(mol)
# mol = pyscf.gto.M(
#     atom=[['O', (0.000000000000, -0.143225816552, 0.000000000000)],
#           ['H', (1.638036840407, 1.136548822547, -0.000000000000)],
#           ['H', (-1.638036840407, 1.136548822547, -0.000000000000)]],
#     #basis='ccpvdz',
#     #basis='6-31G',
#     basis='sto-3g',
#     unit = "bohr",
#     verbose=0,
#     symmetry=False,
# )
#mol.verbose = 4
##############################################################
# Main Code
##############################################################
# print "MOL1 \n"
if __name__ == '__main__':
    # print "\nRHF\n"
    # E,C = RHF(mol1, makeplots = False, debug = False)
    # print "\nUHF\n"
    # E,C_alpha,C_beta = UHF(mol1, makeplots = False, debug = False)
    # print "\nMOL\n"
    # print "\nRHF\n"
    # E,C = RHF(mol, makeplots = False, debug = False)
    print("\nRHF\n")
    E_rhf,C = RHF(mol1, makeplots = False, debug = True, print_hf = False)
    print("E",E_rhf)
    print("C",C)



    print("\n\n\n")
    #print E_rhf,E_uhf,E_huhf
